﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class Player : MonoBehaviour
{
    public float velocidad = 10.0f;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.GetPhotonView().IsMine) {
            if (Input.GetKey(KeyCode.A)) {
              gameObject.transform.position = new Vector3(gameObject.transform.position.x - velocidad * Time.deltaTime, gameObject.transform.position.y, gameObject.transform.position.z);
            }
            
            if (Input.GetKey(KeyCode.D)) {
              gameObject.transform.position = new Vector3(gameObject.transform.position.x + velocidad * Time.deltaTime, gameObject.transform.position.y, gameObject.transform.position.z);
            }
            
            if (Input.GetKey(KeyCode.W)) {
              gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z + velocidad * Time.deltaTime);
            }
            
            if (Input.GetKey(KeyCode.S)) {
              gameObject.transform.position = new Vector3(gameObject.transform.position.x, gameObject.transform.position.y, gameObject.transform.position.z - velocidad * Time.deltaTime);
            }
            
            if (Input.GetKeyDown(KeyCode.T)) {
              EnviarMensaje(PhotonNetwork.NickName);
            }
            
            if (Input.GetKeyDown(KeyCode.C)) {
              CrearAlgoSuperPro();
            }
        }
    }
    
    [PunRPC]
    void RecibirMensaje (string _mensaje) {
      Debug.Log(" Mensaje : " + _mensaje);
    }
    
    void EnviarMensaje (string _mensaje) {
      gameObject.GetPhotonView().RPC("RecibirMensaje", PhotonNetwork.CurrentRoom.GetPlayer(0), _mensaje); // Envia mensaje al jugador 0
      // gameObject.GetPhotonView().RPC("RecibirMensaje", Rpc.Target.All, _mensaje); // envia mensaje a todos los del cuarto
    }
    
    [PunRPC]
    void CrearAlgo () {
      PhotonNetwork.Instantiate("Algo", Vector3.zero, Quaternion.identity);
    }
    
    void CrearAlgoSuperPro () {
      gameObject.GetPhotonView().RPC("CrearAlgo", RpcTarget.AllBufferedViaServer); // Al usar el buffer se guarda durante toda la sesión.
    }
}

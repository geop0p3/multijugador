﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;

public class Nivel : MonoBehaviourPunCallbacks
{
    public Text estado;
    bool empezar = false;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (empezar == false) {
            if (PhotonNetwork.IsMasterClient) {
              estado.text = "Master";
              PhotonNetwork.Instantiate("Player", Vector3.zero + new Vector3(0, 1, 0), Quaternion.identity);
            } else {
              estado.text = "Usuario";
              PhotonNetwork.Instantiate("Player", Vector3.zero + new Vector3(0, 1, 0), Quaternion.identity);
            }
            
            empezar = true;
        }
    }
}

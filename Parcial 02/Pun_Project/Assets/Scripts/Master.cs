﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

using UnityEngine.UI;

public class Master : MonoBehaviourPunCallbacks
{
    public Text status;

    public Button btn_connectToMaster;
    public Button btn_connectToGame;

    private void Awake()
    {
        btn_connectToGame.gameObject.SetActive(false);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        status.text = "Ping : " + PhotonNetwork.GetPing() + " |" + " Conectado : " + PhotonNetwork.IsConnected;
    }

    public void OnClickConnectToServer()
    {
        PhotonNetwork.NickName = "Player001";
        PhotonNetwork.AutomaticallySyncScene = true; //cuando cambia de escena
        PhotonNetwork.GameVersion = "v0";

        PhotonNetwork.ConnectUsingSettings();
    }

    public override void OnConnectedToMaster()
    {
        base.OnConnectedToMaster();
        Debug.Log("Connected to Server Master");

        btn_connectToMaster.gameObject.SetActive(false);
        btn_connectToGame.gameObject.SetActive(true);
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        base.OnDisconnected(cause);
        Debug.Log("Disconnected : " + cause);
    }

    public void OnClickConnectToRoom()
    {
        if (!PhotonNetwork.IsConnected)
            return;

        PhotonNetwork.JoinRandomRoom();
    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        Debug.Log("Joined Room");

        Debug.Log("MASTER : " + PhotonNetwork.IsMasterClient + " | Player Name : " + PhotonNetwork.NickName + " | Room name : " + PhotonNetwork.CurrentRoom.Name + " | Room Players : " + PhotonNetwork.CurrentRoom.PlayerCount); ;
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        base.OnJoinRandomFailed(returnCode, message);
        Debug.Log("Create a new Room");
        PhotonNetwork.CreateRoom("Name Random", new RoomOptions { MaxPlayers = 4, IsVisible = true, IsOpen = true });
    }

    public override void OnCreatedRoom()
    {
        base.OnCreatedRoom();
        Debug.Log("Room Created");
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        base.OnCreateRoomFailed(returnCode, message);
        Debug.Log("Error Create Room : " + message);
    }
}

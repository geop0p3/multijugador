﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Photon.Pun;
using Photon.Realtime;


public class NewLobby : MonoBehaviourPunCallbacks
{
    //Public 
    public string playerName;
    public string gameversion;
    public string roomName;

    List<RoomInfo> CreatedRooms = new List<RoomInfo>();
    // End Public

    Vector2 roomListScroll = Vector2.zero;
    bool joiningRoom = false;

    void Start()
    {
        PhotonNetwork.AutomaticallySyncScene = true;

        if(!PhotonNetwork.IsConnected)
        {
            PhotonNetwork.PhotonServerSettings.AppSettings.AppVersion = gameversion;
            PhotonNetwork.ConnectUsingSettings();
        }
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        base.OnDisconnected(cause);
        Debug.Log("OnfaildToConnect : " + cause.ToString() + " Server : " + PhotonNetwork.ServerAddress);
    }
    public override void OnConnectedToMaster()
    {
        base.OnConnectedToMaster();
        PhotonNetwork.JoinLobby(TypedLobby.Default);
    }
    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        base.OnRoomListUpdate(roomList);
        CreatedRooms = roomList;
    }
    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        base.OnCreateRoomFailed(returnCode, message);
        joiningRoom = false;
    }
    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        base.OnJoinRoomFailed(returnCode, message);
        joiningRoom = false;
    }
    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        base.OnJoinRandomFailed(returnCode, message);
        joiningRoom = false;
    }
    public override void OnCreatedRoom()
    {
        base.OnCreatedRoom();
        Debug.Log("OnCreatedRoom");
        PhotonNetwork.NickName = playerName;

        PhotonNetwork.LoadLevel("Nive00");
    }
    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        Debug.Log("OnJoinedRoom");
        
    }

    void OnGUI()
    {
        GUI.Window(0, new Rect(Screen.width / 2 - 450, Screen.height / 2 - 200, 900, 400), LobbyWindow, "Lobby");
    }

    void LobbyWindow(int index)
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label("Status : " + PhotonNetwork.NetworkClientState);

        if(joiningRoom || !PhotonNetwork.IsConnected || PhotonNetwork.NetworkClientState != ClientState.JoinedLobby)
        {
            GUI.enabled = false;
        }

        GUILayout.FlexibleSpace();

        roomName = GUILayout.TextField(roomName, GUILayout.Width(250));

        if(GUILayout.Button("Create Room",GUILayout.Width(125)))
        {
            if(roomName != "")
            {
                joiningRoom = true;

                RoomOptions roomOptions = new RoomOptions();
                roomOptions.IsOpen = true;
                roomOptions.IsVisible = true;
                roomOptions.MaxPlayers = 5;

                PhotonNetwork.JoinOrCreateRoom(roomName, roomOptions, TypedLobby.Default);
            }
        }

        GUILayout.EndHorizontal();

        roomListScroll = GUILayout.BeginScrollView(roomListScroll, true, true);

        if(CreatedRooms.Count == 0)
        {
            GUILayout.Label("No Rooms created yet...");
        }
        else
        {
            for(int i = 0; i < CreatedRooms.Count; i++)
            {
                GUILayout.BeginHorizontal("box");
                GUILayout.Label(CreatedRooms[i].Name, GUILayout.Width(400));
                GUILayout.Label(CreatedRooms[i].PlayerCount + "/" + CreatedRooms[i].MaxPlayers);

                GUILayout.FlexibleSpace();

                if(GUILayout.Button("Join Room"))
                {
                    joiningRoom = true;

                    PhotonNetwork.NickName = playerName;

                    PhotonNetwork.JoinRoom(CreatedRooms[i].Name);
                }
                GUILayout.EndHorizontal();
            }
        }

        GUILayout.EndScrollView();

        GUILayout.BeginHorizontal();

        GUILayout.Label("Plater Name : ", GUILayout.Width(85));

        playerName = GUILayout.TextField(playerName, GUILayout.Width(250));

        GUILayout.FlexibleSpace();

        GUI.enabled = (PhotonNetwork.NetworkClientState == ClientState.JoinedLobby || PhotonNetwork.NetworkClientState == ClientState.Disconnected) && !joiningRoom;

        if(GUILayout.Button("Refresh",GUILayout.Width(100)))
        {
            if(PhotonNetwork.IsConnected)
            {
                PhotonNetwork.JoinLobby(TypedLobby.Default);
            }
            else
            {
                PhotonNetwork.ConnectUsingSettings();
            }
        }

        GUILayout.EndHorizontal();

        if(joiningRoom)
        {
            GUI.enabled = true;
            GUI.Label(new Rect(900 / 2 - 50, 400 / 2 - 10, 100, 20), "Connecting..");
        }
    }

    void Update()
    {    
    }
}



using UnityEngine.SceneManagement;﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Photon.Pun;
using Photon.Realtime;

public class scr_RequestDB : MonoBehaviour
{
    public InputField user;
    public InputField password;
	public GameObject myPrefab;
	public GameObject ScrollView;

    void Start()
    {
        //StartCoroutine(registro());
    }

    void Update()
    {

    }
    
    public void close(){
    	SceneManager.LoadScene("Lobby", LoadSceneMode.Single);
    }

    public void Registro()
    {
        StartCoroutine(registro());
    }
    
    public void Agregar()
    {
        StartCoroutine(agregar());
    }
    
    public void Listar()
    {
        StartCoroutine(listar());
    }
    
    public void Inicio()
    {
        if((user.text != "" || user.text != null) && (password.text != "" || password.text != null))
            StartCoroutine(inicio());
    }

    IEnumerator registro()
    {
        WWWForm form = new WWWForm();

        form.AddField("user", user.text);
        form.AddField("password", password.text);

        UnityWebRequest www = UnityWebRequest.Post("https://hedronmx.com/createUser.php", form);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);

        }
        else
        {
            Debug.Log("Send Complete");
            if (www.downloadHandler.text == "0") {
              UnityEngine.SceneManagement.SceneManager.LoadScene("Login");
            }
            Debug.Log(www.downloadHandler.text);
        }
    }

    IEnumerator inicio()
    {
        WWWForm form = new WWWForm();

        form.AddField("user", user.text);
        form.AddField("password", password.text);

        UnityWebRequest www = UnityWebRequest.Post("https://hedronmx.com/login.php", form);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);

        }
        else
        {
            PhotonNetwork.NickName = user.text;
            if (www.downloadHandler.text == "Conected1") {
              UnityEngine.SceneManagement.SceneManager.LoadScene("Lobby");
            }
            Debug.Log("Send Complete");
            Debug.Log(www.downloadHandler.text);
        }
    }
    
    IEnumerator agregar()
    {
        WWWForm form = new WWWForm();

        form.AddField("friend", user.text);
        form.AddField("user", password.text);

        UnityWebRequest www = UnityWebRequest.Post("https://hedronmx.com/agregar.php", form);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);

        }
        else
        {
            Debug.Log("Send Complete");
            Debug.Log(www.downloadHandler.text);
        }
    }
    
    IEnumerator listar()
    {
        WWWForm form = new WWWForm();

        form.AddField("user", password.text);

        UnityWebRequest www = UnityWebRequest.Post("https://hedronmx.com/listar.php", form);
        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);

        }
        else
        {
            Debug.Log("Send Complete");
            Debug.Log(www.downloadHandler.text);
            string[] friends = www.downloadHandler.text.Split('\n');
            Debug.Log(friends);
            int space = 40;
            for (int i = 0; i < friends.Length; i++) 
            {
                GameObject newFriend = Instantiate (myPrefab) as GameObject;
                newFriend.transform.SetParent (ScrollView.transform, false);
                newFriend.GetComponentInChildren<Text>().text = friends[i];
                Debug.Log(friends[i]);
                newFriend.transform.position = transform.position + new Vector3(62, space + 25, 0);
                space += 40;
            }
        }
    }
}

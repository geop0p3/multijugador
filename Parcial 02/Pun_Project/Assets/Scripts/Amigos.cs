﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;
using Photon.Realtime;

public class Amigos : MonoBehaviourPunCallbacks
{
    public string gameversion;
    public Text nicknameInput;
    public Text status;
    public Text amigo;
    public string nickname;
    
    public void empezar () {
        nickname = nicknameInput.text;
        PhotonNetwork.AutomaticallySyncScene = true;
        if (PhotonNetwork.IsConnected) {
            PhotonNetwork.NickName = nickname;
            AuthenticationValues values = new AuthenticationValues(nickname);
            PhotonNetwork.AuthValues = values;
            PhotonNetwork.PhotonServerSettings.AppSettings.AppVersion = gameversion;
            PhotonNetwork.ConnectUsingSettings();
        }
    }
    
    public override void OnDisconnected (DisconnectCause cause) {
        base.OnDisconnected(cause);
        Debug.Log("Fail to connect : " + cause.ToString());
    }
    
    public override void OnConnectedToMaster() {
        base.OnConnectedToMaster();
        PhotonNetwork.JoinLobby(TypedLobby.Default);
        
    }
    
    public override void OnJoinedLobby () {
        base.OnJoinedLobby();
        PhotonNetwork.FindFriends(new string[]{amigo.text});
    }
    
    public override void OnFriendListUpdate(List<FriendInfo> friendList) {
        base.OnFriendListUpdate(friendList);
        foreach (FriendInfo info in friendList) {
            Debug.Log("Friend :" + info.UserId + " | online : " + info.IsOnline + " | Room :" + info.Room + " | isR : " + info.IsInRoom);
        }
    }
    
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        status.text = PhotonNetwork.IsConnected.ToString();
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine.UI;

public class Master_Lobby : MonoBehaviourPunCallbacks
{
    public Text nombre1;
    public Text nombre2;
    public Text nombre3;
    public Text nombre4;
    public GameObject nombre2_obj;
    public GameObject nombre3_obj;
    public GameObject nombre4_obj;
    public GameObject menu;
    public Button boton;
    bool empezar = false;

    void Start()
    {
       GameObject.FindGameObjectWithTag("Musica").GetComponent<musica>().StopMusic();
       nombre1.text = PhotonNetwork.NickName;
    }
    
    public void IniciarJuego() {
         menu.SetActive(false);
         gameObject.GetPhotonView().RPC("ClearMenu", RpcTarget.AllViaServer);
    }

    void Update()
    {
        if(empezar == false)
        {
            if (PhotonNetwork.CurrentRoom.PlayerCount >= 2)
            {
                if (PhotonNetwork.IsMasterClient)
                {
                  PhotonNetwork.Instantiate("Male", Vector3.zero + new Vector3(0, 0, 0), Quaternion.Euler(0, 0, 0));
                  nombre2.text = PhotonNetwork.PlayerList[1].NickName;
                  nombre2_obj.SetActive(true);
                  boton.interactable = true;
                }
                else if (PhotonNetwork.CurrentRoom.PlayerCount >= 1 && PhotonNetwork.CurrentRoom.PlayerCount <= 2)
                {
                  boton.interactable = true;
                  nombre2.text = PhotonNetwork.PlayerList[1].NickName;
                  nombre2_obj.SetActive(true);
                  PhotonNetwork.Instantiate("Female", Vector3.zero + new Vector3(1.5f, 0, 0), Quaternion.Euler(0, 0, 0));
                } else if (PhotonNetwork.CurrentRoom.PlayerCount > 2 && PhotonNetwork.CurrentRoom.PlayerCount <= 3) {
                  boton.interactable = true;
                  nombre2.text = PhotonNetwork.PlayerList[1].NickName;
                  nombre2_obj.SetActive(true);
                  nombre3.text = PhotonNetwork.PlayerList[2].NickName;
                  nombre3_obj.SetActive(true);
                  PhotonNetwork.Instantiate("Berserker", Vector3.zero + new Vector3(3f, 0, 0), Quaternion.Euler(0, 0, 0));
                } else if (PhotonNetwork.CurrentRoom.PlayerCount > 3 ) {
                  boton.interactable = true;
                  nombre2.text = PhotonNetwork.PlayerList[1].NickName;
                  nombre2_obj.SetActive(true);
                  nombre3.text = PhotonNetwork.PlayerList[2].NickName;
                  nombre3_obj.SetActive(true);
                  nombre4.text = PhotonNetwork.PlayerList[3].NickName;
                  nombre4_obj.SetActive(true);
                  PhotonNetwork.Instantiate("Heavy", Vector3.zero + new Vector3(-3f, 0, 0), Quaternion.Euler(0, 0, 0));
                }
              empezar = true;
            } else if (PhotonNetwork.CurrentRoom.PlayerCount >= 3) {
              nombre2_obj.SetActive(true);
              nombre3_obj.SetActive(true);
            } else if  (PhotonNetwork.CurrentRoom.PlayerCount >= 4) {
              nombre2_obj.SetActive(true);
              nombre3_obj.SetActive(true);
              nombre4_obj.SetActive(true);
            } else {
              boton.interactable = false;
            }
        }
    }
    
    
    
    [PunRPC]
    void ClearMenu()
    {
        menu.SetActive(false);
    }
}

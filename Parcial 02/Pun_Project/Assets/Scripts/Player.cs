﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;

public class Player : MonoBehaviour
{
    private bool crouchBool = false;
	  private bool blockBool = false;
	  private bool dead = false;
	  private bool InAir = false;
	  public Animator animator;
    public float rotSpeed = 360f;
    public float rot = 0f;
	  private Transform defaultCamTransform;
	  private Vector3 resetPos;
	  private Quaternion resetRot;
	  private GameObject cam;
	  private GameObject fighter;
    public float velocidad = 1f;
    public Vector3 moveDir = Vector3.zero;
    public float gravity = 8f;
    public bool moving = false;
    public float vida = 70;
    public bool muerto = false;
    public GameObject puno;
    CharacterController controller;

    // Start is called before the first frame update
    void Start()
    {
      cam = GameObject.FindWithTag("MainCamera");
		  defaultCamTransform = cam.transform;
		  resetPos = defaultCamTransform.position;
		  resetRot = defaultCamTransform.rotation;
		  fighter = GameObject.FindWithTag("Player");
		  fighter.transform.position = new Vector3(0,0,0);
		  animator.SetTrigger("Intro2Trigger");
		  controller = GetComponent<CharacterController> ();
    }
    
    IEnumerator COInAir(float toAnimWindow)
	  {
		  yield return new WaitForSeconds(toAnimWindow);
		  InAir = true;
		  animator.SetBool("InAir", true);
		  yield return new WaitForSeconds(0.5f);
		  InAir = false;
		  animator.SetBool("InAir", false);
	  }

    // Update is called once per frame
    void Update()
    {

        if (gameObject.GetPhotonView().IsMine && muerto == false)
        {
        
            if (Input.GetKeyDown(KeyCode.W))
            {
                if (!InAir) {
                  animator.SetBool("Walk Forward", true);
                }
            }
        
            if (Input.GetKey(KeyCode.W))
            {
                moving = true;
                moveDir = new Vector3 (0, 0, 1);
                moveDir *= velocidad;
                moveDir = transform.TransformDirection(moveDir);
                if (Input.GetKeyDown(KeyCode.E)) {
                    animator.SetBool("Walk Forward", false);
                    animator.SetTrigger("MoveAttack2Trigger");
                    puno.GetComponent<Collider>().enabled = true;
                }
                
                if (Input.GetKeyDown(KeyCode.Q)) {
                    animator.SetBool("Walk Forward", false);
                    animator.SetTrigger("MoveAttack1Trigger");
                }
                
                
                if (Input.GetKeyDown(KeyCode.Space)) {
                    animator.SetBool("Walk Forward", false);
                    animator.SetTrigger("JumpForwardTrigger");
		            StartCoroutine (COInAir(0.25f));
                }
                
            }
            if (Input.GetKeyUp(KeyCode.W))
            {
                moveDir = new Vector3 (0, 0, 0);
                animator.SetBool("Walk Forward", false);
                moving = false;
            }
            
            
            if (Input.GetKeyDown(KeyCode.S))
            {
               if (!InAir) {
                  animator.SetBool("Walk Backward", true);
                }
            }
            
            if (Input.GetKeyUp(KeyCode.S))
            {
               moveDir = new Vector3 (0, 0, 0);
               animator.SetBool("Walk Backward", false);
               moving = false;
            }
            
            if (Input.GetKey(KeyCode.S))
            {
                moving = true;
                moveDir = new Vector3 (0, 0, -1);
                moveDir *= velocidad;
                moveDir = transform.TransformDirection(moveDir);
                if (Input.GetKeyDown(KeyCode.Space)) {
                    animator.SetTrigger("JumpBackwardTrigger");
					          StartCoroutine (COInAir(0.25f));
                    animator.SetBool("Walk Backward", false);
                }
            }
            
            if (Input.GetKeyDown(KeyCode.Space))
            {
              if (!moving) {
                animator.SetTrigger("JumpTrigger");
		        StartCoroutine (COInAir(0.25f));
              }
            }
            
            if (Input.GetKeyDown(KeyCode.E)) {
                if (!moving) {
                  animator.SetTrigger("JabTrigger");
                  puno.GetComponent<Collider>().enabled = true;
                }
            }
            
            if (Input.GetKeyDown(KeyCode.Q)) {
                  if (!moving) {
                    animator.SetTrigger("PunchTrigger");
                  }
                }
            
            
            rot += Input.GetAxis ("Horizontal") * rotSpeed * Time.deltaTime;
            transform.eulerAngles = new Vector3 (0, rot, 0);
            moveDir.y -= gravity * Time.deltaTime;
            
            controller.Move (moveDir * Time.deltaTime);
        }
    }
    
    public void CallDano() {
      Debug.Log("Entro Call Dano");
      gameObject.GetPhotonView().RPC("RecibirDano", RpcTarget.AllBufferedViaServer);
    }
    
    void Muerte() {
        animator.SetTrigger("DeathTrigger");
        muerto = true;
        gameObject.GetPhotonView().RPC("RestartGame", RpcTarget.AllViaServer);
    }
    
    void OnTriggerEnter(Collider collision)
    {   
        Debug.Log("EntroTrigger");
        if (collision.gameObject.tag != gameObject.tag) {
            if (vida > 0) {
                animator.SetTrigger("LightHitTrigger");
                vida -= 10;
                gameObject.GetPhotonView().RPC("CallVida", RpcTarget.AllViaServer, vida);
                Debug.Log(vida);
            } else {
                Muerte();
            }
        }
    }


    [PunRPC]
    void CallVida(int vidax)
    {
        vida = vidax;
        if (vida <= 0){
        Muerte();
        }
    }
    
    [PunRPC]
    void RestartGame()
    {
            PhotonNetwork.LoadLevel("Nive00");
    }
}

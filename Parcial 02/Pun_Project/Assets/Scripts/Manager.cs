﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using UnityEngine.SceneManagement;
using Photon.Realtime;

public class Manager : MonoBehaviourPunCallbacks
{
    public string playerName = "Jugador";
    public string gameVersion;
    public string roomName;
    public InputField roomNombre;
    public InputField nombreJugador;
    public Text statusNetwork;
    public Font font;

    List<RoomInfo> CreatedRooms = new List<RoomInfo>();

    Vector2 roomListScroll = Vector2.zero;
    bool joiningRoom = false;

    // Start is called before the first frame update
    void Start()
    {
        nombreJugador.text = PhotonNetwork.NickName;
        PhotonNetwork.AutomaticallySyncScene = true;
        if (!PhotonNetwork.IsConnected)
        {
            PhotonNetwork.PhotonServerSettings.AppSettings.AppVersion = gameVersion;
            PhotonNetwork.ConnectUsingSettings();
        }
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        base.OnDisconnected(cause);
        Debug.Log("On failed to connect: " + cause.ToString() + "Server : " + PhotonNetwork.ServerAddress);
    }
    
    public void AbrirAmigos() {
        SceneManager.LoadScene("Friends", LoadSceneMode.Single);
    }
    
    public void OnClickJoinRandomRoom()
    {
        joiningRoom = true;
        if (CreatedRooms.Count > 0)
        {
            PhotonNetwork.JoinRandomRoom();
        } 
        else
        {
            RoomOptions roomOptions = new RoomOptions();
            roomOptions.IsOpen = true;
            roomOptions.IsVisible = true;
            roomOptions.MaxPlayers = 2;

            PhotonNetwork.JoinOrCreateRoom("Random " + CreatedRooms.Count.ToString(), roomOptions, TypedLobby.Default);
        }
    }

    public override void OnConnectedToMaster()
    {
        base.OnConnectedToMaster();
        PhotonNetwork.JoinLobby(TypedLobby.Default);
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        base.OnRoomListUpdate(roomList);
        CreatedRooms = roomList;
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        base.OnCreateRoomFailed(returnCode, message);
        joiningRoom = false;
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        base.OnJoinRoomFailed(returnCode, message);
        joiningRoom = false;
    }

    public override void OnJoinRandomFailed(short returnCode, string message)
    {
        base.OnJoinRandomFailed(returnCode, message);
        joiningRoom = false;
    }

    public override void OnCreatedRoom()
    {
        base.OnCreatedRoom();
        Debug.Log("OnCreatedRoom");

        PhotonNetwork.NickName = playerName;
        PhotonNetwork.LoadLevel("Nivel0");
    }

    public override void OnJoinedRoom()
    {
        base.OnJoinedRoom();
        Debug.Log("OnJoinedRoom");
    }

    void OnGUI()
    {
        GUI.Window(0, new Rect(Screen.width / 2 - Screen.width * 0.375f , Screen.height / 2 - Screen.width * 0.14f, Screen.width * 0.75f, Screen.height * 0.42f), LobbyWindow, "Lista de Cuartos");
    }
    
    public void CreateRoom () {
      if(roomName != "")
            {
                joiningRoom = true;

                RoomOptions roomOptions = new RoomOptions();
                roomOptions.IsOpen = true;
                roomOptions.IsVisible = true;
                roomOptions.MaxPlayers = 4;

                PhotonNetwork.JoinOrCreateRoom(roomName, roomOptions, TypedLobby.Default);
            }
    }

    void LobbyWindow(int index)
    {
        GUI.skin.font = font;
        GUILayout.BeginHorizontal();

        if(joiningRoom || !PhotonNetwork.IsConnected || PhotonNetwork.NetworkClientState != ClientState.JoinedLobby)
        {
            GUI.enabled = false;
        }

        GUILayout.FlexibleSpace();


        GUILayout.EndHorizontal();

        roomListScroll = GUILayout.BeginScrollView(roomListScroll, true, true);

        if (CreatedRooms.Count == 0)
        {
            GUILayout.Label("Sin Cuartos");
        }
        else
        {
            for(int i = 0; i < CreatedRooms.Count; i++)
            {
                GUILayout.BeginHorizontal("box");
                GUILayout.Label(CreatedRooms[i].Name, GUILayout.Width(400));
                GUILayout.Label(CreatedRooms[i].PlayerCount + "/" + CreatedRooms[i].MaxPlayers);

                GUILayout.FlexibleSpace();

                if (GUILayout.Button("Unirse"))
                {
                    joiningRoom = true;

                    PhotonNetwork.NickName = playerName;

                    PhotonNetwork.JoinRoom(CreatedRooms[i].Name);
                }

                GUILayout.EndHorizontal();
            }
        }

        GUILayout.EndScrollView();
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUI.enabled = (PhotonNetwork.NetworkClientState == ClientState.JoinedLobby || PhotonNetwork.NetworkClientState == ClientState.Disconnected) && !joiningRoom;

        GUILayout.EndHorizontal();

        if (joiningRoom)
        {
            GUI.enabled = true;
            GUI.Label(new Rect(800 / 2 - 50, 300 / 2 - 10, 200, 20), "Conectando..");
        }
    }
    
    public void Actualizar () {
      if (PhotonNetwork.IsConnected)
            {
                PhotonNetwork.JoinLobby(TypedLobby.Default);
            }
            else
            {
                PhotonNetwork.ConnectUsingSettings();
            }
    }

    // Update is called once per frame
    void Update()
    {
        statusNetwork.text = "Status : " + PhotonNetwork.NetworkClientState;
        roomName = roomNombre.text;
        playerName = nombreJugador.text;
    }
}

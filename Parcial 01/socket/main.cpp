# include <iostream>
# include "socket.h"

using namespace std;

int main () {
  int port = 8080;
  int opcion = -1;
  string ipAddress;
  bool fin = false;
  char recMessage[STRLEN];
  char sendMessage[STRLEN];
  
  cout << "1) Server\n";
  cout << "2) Cliente\n";
  cout << "3) Salir\n";
  cin >> opcion;
  
  if (opcion == 1){
    ServerSocket sockServer;
    cout << "Hosting\n";
    sockServer.StartHosting(port);
    
    while (!fin){
      cout << "\t Esperando\n";
      sockServer.RecvData(recMessage,STRLEN);
      cout << " Recibido > " << recMessage << "\n";
      sockServer.GetAndSendMessage();
      if (strcmp (recMessage, "salir") == 0 || strcmp (sendMessage, "salir") == 0){
        fin = true;
      }
      sockServer.CloseConnection();
      
    }
  } else if (opcion == 2){
    cout << "IP:\n";
    cin >> ipAddress;
    ClientSocket sockClient;
    cout << "Conectando\n";
    sockClient.ConnectToServer(ipAddress.c_str(), port);
    
    while (!fin){
      sockClient.GetAndSendMessage();
      cout << "\t Esperando\n";
      sockClient.RecvData(recMessage, STRLEN);
      cout << "Recibido > " << recMessage << "\n";
      if (strcmp (recMessage, "salir") == 0 || strcmp (sendMessage, "salir") == 0){
        fin = true;
      }
    }
    
    sockClient.CloseConnection();
    
  } else {
    exit (0);
    
  }
  
  return 0;
}

#include <iostream>
#include <thread>
#include <windows.h>
#include <ctime>
#include <string>
#include <cstring>
#include <sstream>
#include <cstdlib>

#include "Socket.h"

using namespace std;

HANDLE hOut;

const int delay = 50;
int width = 55;
int height = 40;

const float maxSpeed = 1.8f;

int ServerScore = 0;
int ClienteScore = 0;

bool fin = false;
char recMessage[STRLEN];
char sendMessage[STRLEN];

ServerSocket sockServer;
ClientSocket sockClient;

struct pad
{
    float x, y;
    int sizeP;
    float ox, oy;
    float vx;

    pad(int _x, int _y, int _size)
    {
        x = _x;
        y = _y;
        sizeP = _size;
        ox = x;
        oy = y;
    }

    void draw()
    {
        COORD p;
        p.X = ox - sizeP;
        p.Y = oy;
        SetConsoleCursorPosition(hOut, p);

        for(int i = -sizeP; i < sizeP; i++)
            cout << ' ';

        p.X = x-sizeP;
        p.Y = y;
        SetConsoleCursorPosition(hOut, p);
        SetConsoleTextAttribute(hOut, 10);

        for(int i = -sizeP; i < sizeP; i++)
            cout << '=';

        SetConsoleTextAttribute(hOut, 15);
        ox = x;
        oy = y;
    }
};

struct ball
{
    float x, y, ox, oy;
    float vx, vy;

    ball(int _x, int _y)
    {
        x = _x;
        y = _y;
        ox = x;
        oy = y;
        vx = vy = 0;
    }

    void draw()
    {
        COORD p;
        p.X = ox;
        p.Y = oy;

        SetConsoleCursorPosition(hOut, p);
        cout << ' ';

        p.X = x;
        p.Y = y;

        SetConsoleCursorPosition(hOut, p);
        SetConsoleTextAttribute(hOut, 12);
        cout << '0';

        SetConsoleTextAttribute(hOut, 15);
        ox = x;
        oy = y;
    }
};

pad SERVER (width/2, height-2, 4);
pad CLIENTE (width/2, 2, 4);
ball PELOTA (width/2-1, height/2);

void PlayerServer()
{
    do
    {
        if(GetAsyncKeyState(VK_LEFT))
            SERVER.x -= 2;
        else if(GetAsyncKeyState(VK_RIGHT))
            SERVER.x += 2;

        if(SERVER.x < SERVER.sizeP+1)
            SERVER.x = SERVER.sizeP+1;
        else if(SERVER.x > width - SERVER.sizeP-2)
            SERVER.x = width - SERVER.sizeP-2;

        this_thread::sleep_for(chrono::milliseconds(50));

        stringstream data_temp;
        data_temp << SERVER.x;
        string data = "_PJP:" + data_temp.str();

        char csrt[data.size()+1];
        copy(data.begin(), data.end(), csrt);
        csrt[data.size()] = '\0';

        sockServer.SendData(csrt);

    }while(fin == false);
}

void PlayerCliente()
{
    do
    {
        if(GetAsyncKeyState('A'))
            CLIENTE.x -= 2;
        else if(GetAsyncKeyState('D'))
            CLIENTE.x += 2;

        if(CLIENTE.x < CLIENTE.sizeP+1)
            CLIENTE.x = CLIENTE.sizeP+1;
        else if(CLIENTE.x > width - CLIENTE.sizeP-2)
            CLIENTE.x = width - CLIENTE.sizeP-2;

        this_thread::sleep_for(chrono::milliseconds(50));

        stringstream data_temp;
        data_temp << CLIENTE.x;
        string data = "_PJP:" + data_temp.str();

        char csrt[data.size()+1];
        copy(data.begin(), data.end(), csrt);
        csrt[data.size()] = '\0';

        sockClient.SendData(csrt);

    }while(fin == false);
}

void RecibirMensajeCliente()
{
    do
    {
        sockClient.RecvData(recMessage, STRLEN);

        if(strncmp(recMessage, "_PJP:", 5) == 0)
        {
            string rec = recMessage;
            int pos = rec.find(":");
            string coord = rec.substr(pos+1);

            stringstream geek(coord);
            int _x = 0;
            geek >> _x;
            SERVER.x = _x;
        }
        else if(strncmp(recMessage, "_PEX:", 5) == 0)
        {
            string rec = recMessage;
            int pos = rec.find(":");
            string coord = rec.substr(pos+1);

            stringstream geek(coord);
            int _x = 0;
            geek >> _x;
            PELOTA.x = _x;
        }
        else if(strncmp(recMessage, "_PEY:", 5) == 0)
        {
            string rec = recMessage;
            int pos = rec.find(":");
            string coord = rec.substr(pos+1);

            stringstream geek(coord);
            int _y = 0;
            geek >> _y;
            PELOTA.y = _y;
        }

    }while(fin == false);
}

void RecibirMensajeServer()
{
    do
    {
        sockServer.RecvData(recMessage, STRLEN);

        if(strncmp(recMessage, "_PJP:", 5) == 0)
        {
            string rec = recMessage;
            int pos = rec.find(":");
            string coord = rec.substr(pos+1);

            stringstream geek(coord);
            int _x = 0;
            geek >> _x;
            CLIENTE.x = _x;
        }

    }while(fin == false);
}

void drawMarco()
{
    SetConsoleTextAttribute(hOut, 15);
    for(int i = 0; i < width-1; i++)
        cout << '-';
    for(int i = 0; i < height-1; i++)
    {
        cout << "\n|";
        for(int i = 0; i < width-3; i++)
            cout << ' ';
        cout << "|";
    }
    cout << "\n";
}

void reset(ball &PELOTA, int &_ServerScore, int &_ClienteScore)
{
    PELOTA.x = width/2-1;
    PELOTA.y = height/2;

    PELOTA.vx = (rand()%3)-1.5f;
    PELOTA.vy = 1;

    COORD p;
    p.X = 0;
    p.Y = height;
    SetConsoleCursorPosition(hOut, p);
    cout <<  "\n\tServer : " << ClienteScore << "\tClient : " << ServerScore << "\n";

}

void movePelota()
{
    do
    {
       if(PELOTA.vx < -maxSpeed)
            PELOTA.vx = -maxSpeed;
        else if(PELOTA.vx > maxSpeed)
            PELOTA.vx = maxSpeed;

        PELOTA.x += PELOTA.vx;
        PELOTA.y += PELOTA.vy;

        if(PELOTA.y >= height)
        {
            ++ServerScore;
            reset(PELOTA, ServerScore, ClienteScore);
        }
        else if(PELOTA.y <= 0)
        {
            ++ClienteScore;
            reset(PELOTA, ServerScore, ClienteScore);
        }

        if(PELOTA.x <= 1)
        {
            PELOTA.vx *= -1;
            PELOTA.x = 1.0f;
        }
        else if(PELOTA.x >= width-3)
        {
            PELOTA.vx *= -1;
            PELOTA.x = width-3;
        }

        if(PELOTA.y >= SERVER.y-1 && PELOTA.y <= SERVER.y +1)
        {
            if(PELOTA.x > SERVER.x - SERVER.sizeP && PELOTA.x < SERVER.x + SERVER.sizeP)
            {
                PELOTA.vy *= -1;
                PELOTA.vx += (PELOTA.x - SERVER.x)/3;
                PELOTA.y = SERVER.y -1;
            }
        }

        if(PELOTA.y <= CLIENTE.y+1 && PELOTA.y >= CLIENTE.y -1)
        {
            if(PELOTA.x > CLIENTE.x - CLIENTE.sizeP && PELOTA.x < CLIENTE.x + CLIENTE.sizeP)
            {
                PELOTA.vy *= -1;
                PELOTA.vx += (PELOTA.x - CLIENTE.x)/3;
                PELOTA.y = CLIENTE.y +1;
            }
        }

        this_thread::sleep_for(chrono::milliseconds(40));

        stringstream data_tempX;
        data_tempX << PELOTA.x;
        string dataX = "_PEX:" + data_tempX.str();

        char csrtX[dataX.size()+1];
        copy(dataX.begin(), dataX.end(), csrtX);
        csrtX[dataX.size()] = '\0';

        sockServer.SendData(csrtX);

        this_thread::sleep_for(chrono::milliseconds(40));

        stringstream data_tempY;
        data_tempY << PELOTA.y;
        string dataY = "_PEY:" + data_tempY.str();

        char csrtY[dataY.size()+1];
        copy(dataY.begin(), dataY.end(), csrtY);
        csrtY[dataY.size()] = '\0';

        sockServer.SendData(csrtY);

    }while(fin == false);
}

int main()
{
    hOut = GetStdHandle(STD_OUTPUT_HANDLE);
    SMALL_RECT DisplayArea = {0, 0, width, height+3};
    SetConsoleWindowInfo(hOut, TRUE, &DisplayArea);

    CONSOLE_CURSOR_INFO ConCurInf;
    ConCurInf.dwSize = 10;
    ConCurInf.bVisible = false;
    SetConsoleCursorInfo(hOut, &ConCurInf);

    drawMarco();

    int port = 666;
    int opcion = -1;

    string ipAddress = "127.0.0.1";


    cout <<  "Server : " << ClienteScore << "\tClient : " << ServerScore << "\n";
    cout << "1) Server\n";
    cout << "2) Cliente\n";
    cout << "3) Salir\n";

    cin >> opcion;

    switch(opcion)
    {
        case 1:
        {

            cout << "HOSTING\n";
            sockServer.StartHosting(port);

            thread thrServer(RecibirMensajeServer);
            thread thrServerMOV(PlayerServer);

            PELOTA.vx = float(rand()%3)-1.5f;
            PELOTA.vy = 1;

            thread thrMovPelota(movePelota);

            while(!fin)
            {

                SERVER.draw();
                CLIENTE.draw();
                Sleep(delay);
                PELOTA.draw();
                //sockServer.GetAndSendMessage();
            }
            sockServer.CloseConnection();
        }
        break;

        case 2:
        {
            //cout << "IP:\n";
            //cin >> ipAddress;


            cout << "Conectado \n";
            sockClient.ConnectToServer(ipAddress.c_str(), port);

            thread thrCliente(RecibirMensajeCliente);
            thread thrClienteMOV(PlayerCliente);

            while(!fin)
            {
                //sockClient.GetAndSendMessage();
                SERVER.draw();
                CLIENTE.draw();
                Sleep(delay);
                PELOTA.draw();
            }
            sockClient.CloseConnection();
        }
        break;

        default:
        {
            exit(0);
        }
        break;
    }

    return 0;
}

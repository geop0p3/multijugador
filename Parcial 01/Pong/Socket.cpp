#include "Socket.h"

Socket::Socket(){
    if(WSAStartup(MAKEWORD(2,2), &wsaData) != NO_ERROR){
        cerr<<"Socket Init : Error WSASStart\n";
        system("pause");
        WSACleanup();
        exit(10);
    }

    mySocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    if(mySocket == INVALID_SOCKET){
        cerr<<"Error create socket\n";
        system("pause");
        WSACleanup();
        exit(11);
    }

    myBackup = mySocket;
}

Socket::~Socket(){
    WSACleanup();
}

bool Socket::SendData(char *_buffer){
    send(mySocket, _buffer, strlen(_buffer), 0);
    return true;
}

bool Socket::SendBit(string _data, char* _HEAD){
    char char_temp[5];
    strcpy(char_temp, _HEAD);
    strcat(char_temp, _data.c_str());

    send(mySocket, char_temp, strlen(char_temp), 0);
    return true;
}

bool Socket::RecvData(char *_buffer, int _size){
    int i = recv(mySocket, _buffer, _size, 0);
    _buffer[i] = '\0';
    return true;
}

void Socket::CloseConnection(){
    closesocket(mySocket);
    mySocket = myBackup;
}

void Socket::GetAndSendMessage(){
    char message[STRLEN];

    cin.ignore();
    cout<<"Enviar >";
    cin.get(message, STRLEN);
    SendData(message);

    /*char message[STRLEN];
    bool loop = true;

    do{
        cin.ignore();
        cout<<"Enviar >";

        cout<<strcmp(message, "Piedra") <<"\n";
        cout<<strcmp(message, "Tijeras") <<"\n";
        cout<<strcmp(message, "Papel") <<"\n";



    }while(loop == true);*/
}

void ServerSocket::Listen(){
    if(listen(mySocket, 1)==SOCKET_ERROR){
        cerr<<"Error listen\n";
        system("pause");
        WSACleanup();
        exit(15);
    }

    acceptSocket = accept(myBackup, NULL, NULL);
    while(acceptSocket == SOCKET_ERROR){
        acceptSocket = accept(myBackup, NULL, NULL);
    }

    mySocket = acceptSocket;
}

void ServerSocket::Bind(int _port){
    myAddress.sin_family = AF_INET;
    myAddress.sin_addr.s_addr = inet_addr("0.0.0.0");
    myAddress.sin_port = htons(_port);

    if(bind(mySocket, (SOCKADDR*)&myAddress, sizeof(myAddress)) == SOCKET_ERROR){
        cerr<<"Failed to connect\n";
        system("pause");
        WSACleanup();
        exit(14);
    }
}

void ClientSocket::ConnectToServer(const char*_ipAddress, int _port){
    myAddress.sin_family = AF_INET;
    myAddress.sin_addr.s_addr = inet_addr(_ipAddress);
    myAddress.sin_port = htons(_port);

    if(connect(mySocket, (SOCKADDR*)&myAddress, sizeof(myAddress)) == SOCKET_ERROR){
        cerr<<"Fail to connect\n";
        system("pause");
        WSACleanup();
        exit(13);
    }
}

void ServerSocket::StartHosting(int port){
    Bind(port);
    Listen();
}

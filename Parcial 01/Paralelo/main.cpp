#include <iostream>
#include <thread>

using namespace std;

void myHilo(int _num){
  for (int i = 0; i < _num; i++){
    cout << "Hilo : " << _num << "\n";
  }
}

class c_myHilo{
  public:
    void operator () (int _num){
      for (int i = 0; i < 0; i++){
        cout << "Hilo: " << i << "\n";
      }
    }
};

int main (){
  int valor = 20;
  for (int i = 0; i < valor; i++){
     cout << "Loop for : " << valor << "\n";
  }
  
  thread hilo1(myHilo, 500); // Usando threads
  hilo1.join();
  
  auto f = [](int x){
    for (int i = 0; i < x; i++){
     cout << "Lambda: " << i << "\n";
    }
  };
  
  thread thlambda(f,30);
  thlambda.join();
  
  return 0; 
}

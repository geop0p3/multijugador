#include <iostream>
#include "socket.h"
#include <ctype.h>
#include <stdlib.h>

using namespace std;

char matrix3[3][3] = {'1', '2', '3', '4', '5', '6', '7', '8', '9'};
char matrix5[5][5] = {'1', '2', '3', '4', '5', '6', '7', '8', '9',
                      'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i',
                      'j', 'k', 'l', 'm', 'n', 'o', 'p'};
char player1 = 'X';
char player2 = 'O';
char player = 'X';
char mododejuego = '0';
void Draw() {
  system ("cls");
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      cout << matrix3[i][j] << " ";
    }
    cout << endl;
  }
}

void Draw5() {
  system ("cls");
  for (int i = 0; i < 5; i++) {
    for (int j = 0; j < 5; j++) {
      cout << matrix5[i][j] << " ";
    }
    cout << endl;
  }
}

int turno = 0;

void Input (char a) {
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      if (a == matrix3[i][j]) {
        matrix3[i][j] = player;
      }
    }
    cout << endl;
  }
}

void Input5 (char a) {
  for (int i = 0; i < 5; i++) {
    for (int j = 0; j < 5; j++) {
      if (a == matrix5[i][j]) {
        matrix5[i][j] = player;
      }
    }
    cout << endl;
  }
}

void CambiarTurno () {
  if (player == player1) {
    player = player2;
  }
  else {
    player = player1;
  }
  
}

char Gano () {
  if (matrix3[0][0] == player1 && matrix3[0][1] == player1 &&
      matrix3[0][2] == player1)
    return player1;
  if (matrix3[1][0] == player1 && matrix3[1][1] == player1 &&
      matrix3[1][2] == player1)
    return player1;
  if (matrix3[2][0] == player1 && matrix3[2][1] == player1 &&
      matrix3[2][2] == player1)
    return player1;

  if (matrix3[0][0] == player1 && matrix3[1][0] == player1 &&
      matrix3[2][0] == player1)
    return player1;
  if (matrix3[0][1] == player1 && matrix3[1][1] == player1 &&
      matrix3[2][1] == player1)
    return player1;
  if (matrix3[0][2] == player1 && matrix3[1][2] == player1 &&
      matrix3[2][2] == player1)
    return player1;

  if (matrix3[0][0] == player1 && matrix3[1][1] == player1 &&
      matrix3[2][2] == player1)
    return player1;
  if (matrix3[2][0] == player1 && matrix3[1][1] == player1 &&
      matrix3[0][2] == player1)
    return player1;

  if (matrix3[0][0] == player2 && matrix3[0][1] == player2 &&
      matrix3[0][2] == player2)
    return player2;
  if (matrix3[1][0] == player2 && matrix3[1][1] == player2 &&
      matrix3[1][2] == player2)
    return player2;
  if (matrix3[2][0] == player2 && matrix3[2][1] == player2 &&
      matrix3[2][2] == player2)
    return player2;

  if (matrix3[0][0] == player2 && matrix3[1][0] == player2 &&
      matrix3[2][0] == player2)
    return player2;
  if (matrix3[0][1] == player2 && matrix3[1][1] == player2 &&
      matrix3[2][1] == player2)
    return player2;
  if (matrix3[0][2] == player2 && matrix3[1][2] == player2 &&
      matrix3[2][2] == player2)
    return player2;

  if (matrix3[0][0] == player2 && matrix3[1][1] == player2 &&
      matrix3[2][2] == player2)
    return player2;
  if (matrix3[2][0] == player2 && matrix3[1][1] == player2 &&
      matrix3[0][2] == player2)
    return player2;

  return '/';
}

char Gano5() { 
  if (matrix5[0][0] == player1 && matrix5[1][0] == player1 && matrix5[2][0] == player1 && matrix5[3][0] == player1 && matrix5[4][0] == player1) {
    return player1;
  } else if (matrix5[0][0] == player2 && matrix5[1][0] == player2 && matrix5[2][0] == player2 && matrix5[3][0] == player2 && matrix5[4][0] == player2) {
    return player2;
  }
  
  if (matrix5[0][1] == player1 && matrix5[1][1] == player1 && matrix5[2][1] == player1 && matrix5[3][1] == player1 && matrix5[4][1] == player1) {
    return player1;
  } else if (matrix5[0][1] == player2 && matrix5[1][1] == player2 && matrix5[2][1] == player2 && matrix5[3][1] == player2 && matrix5[4][1] == player2) {
    return player2;
  }
  
  if (matrix5[0][2] == player1 && matrix5[1][2] == player1 && matrix5[2][2] == player1 && matrix5[3][2] == player1 && matrix5[4][2] == player1) {
    return player1;
  } else if (matrix5[0][2] == player2 && matrix5[1][2] == player2 && matrix5[2][2] == player2 && matrix5[3][2] == player2 && matrix5[4][2] == player2) {
    return player2;
  }
  
  if (matrix5[0][3] == player1 && matrix5[1][3] == player1 && matrix5[2][3] == player1 && matrix5[3][3] == player1 && matrix5[4][3] == player1) {
    return player1;
  } else if (matrix5[0][3] == player2 && matrix5[1][3] == player2 && matrix5[2][3] == player2 && matrix5[3][3] == player2 && matrix5[4][3] == player2) {
    return player2;
  }
  
  if (matrix5[0][4] == player1 && matrix5[1][4] == player1 && matrix5[2][4] == player1 && matrix5[3][4] == player1 && matrix5[4][4] == player1) {
    return player1;
  } else if (matrix5[0][4] == player2 && matrix5[1][4] == player2 && matrix5[2][4] == player2 && matrix5[3][4] == player2 && matrix5[4][4] == player2) {
    return player2;
  }
  
  
  
  if (matrix5[0][0] == player1 && matrix5[0][1] == player1 && matrix5[0][2] == player1 && matrix5[0][3] == player1 && matrix5[0][4] == player1) {
    return player1;
  } else if (matrix5[0][0] == player2 && matrix5[0][1] == player2 && matrix5[0][2] == player2 && matrix5[0][3] == player2 && matrix5[0][4] == player2) {
    return player2;
  }
  
  if (matrix5[1][0] == player1 && matrix5[1][1] == player1 && matrix5[1][2] == player1 && matrix5[1][3] == player1 && matrix5[1][4] == player1) {
    return player1;
  } else if (matrix5[1][0] == player2 && matrix5[1][1] == player2 && matrix5[1][2] == player2 && matrix5[1][3] == player2 && matrix5[1][4] == player2) {
    return player2;
  }
  
  if (matrix5[2][0] == player1 && matrix5[2][1] == player1 && matrix5[2][2] == player1 && matrix5[2][3] == player1 && matrix5[2][4] == player1) {
    return player1;
  } else if (matrix5[2][0] == player2 && matrix5[2][1] == player2 && matrix5[2][2] == player2 && matrix5[2][3] == player2 && matrix5[2][4] == player2) {
    return player2;
  }
  
  if (matrix5[3][0] == player1 && matrix5[3][1] == player1 && matrix5[3][2] == player1 && matrix5[3][3] == player1 && matrix5[3][4] == player1) {
    return player1;
  } else if (matrix5[3][0] == player2 && matrix5[3][1] == player2 && matrix5[3][2] == player2 && matrix5[3][3] == player2 && matrix5[3][4] == player2) {
    return player2;
  }
  
  if (matrix5[4][0] == player1 && matrix5[4][1] == player1 && matrix5[4][2] == player1 && matrix5[4][3] == player1 && matrix5[4][4] == player1) {
    return player1;
  } else if (matrix5[4][0] == player2 && matrix5[4][1] == player2 && matrix5[4][2] == player2 && matrix5[4][3] == player2 && matrix5[4][4] == player2) {
    return player2;
  }
  
  
  
  if (matrix5[4][0] == player1 && matrix5[3][1] == player1 && matrix5[2][2] == player1 && matrix5[1][3] == player1 && matrix5[0][4] == player1) {
    return player1;
  } else if (matrix5[4][0] == player2 && matrix5[3][1] == player2 && matrix5[2][2] == player2 && matrix5[1][3] == player2 && matrix5[0][4] == player2) {
    return player2;
  }
  
  return '/';
}

int main() {
  int port = 8080;
  int opcion = -1;
  string ipAddress;
  bool fin = false;
  char recMessage[STRLEN];
  char sendMessage[STRLEN];

  cout << "1) Server\n";
  cout << "2) Cliente\n";
  cout << "3) Salir\n";
  cin >> opcion;

  if (opcion == 1) {
    ServerSocket sockServer;
    cout << "Hosting\n";
    sockServer.StartHosting(port);
    cout << "Presiona 3 para jugar modo 3x3, Presiona 5 para jugar modo 5x5" << endl;
    cin >> mododejuego;
    sockServer.SendPlayer(mododejuego);
    cout << "\t Esperando\n";
    sockServer.RecvData(recMessage, STRLEN);
    player1 = recMessage[0];
    cout << "Jugador 1: " << player1 << endl;
    cout << "Ingresa tu simbolo " << endl;
    cin >> player2;
    sockServer.SendPlayer(player2);
    player = player1;

    while (!fin) {
    
      if (mododejuego != '5') {
        Draw();
      } else {
        Draw5();
      }
      
      cout << "\t Esperando\n";
      sockServer.RecvData(recMessage, STRLEN);
      cout << " Recibido > " << recMessage << "\n";
      
      if (mododejuego != '5') {
        Input(recMessage[0]);
      } else {
        Input5(recMessage[0]);
      }
      
      turno++;
      
      if (mododejuego != '5') {
        Draw();
      } else {
        Draw5();
      }

      if (mododejuego == '5') {
        if (Gano5() == player1) {
          cout << "Gano el jugador " << player1 << endl;
          fin = true;
        } else if (Gano5() == player2) {
          cout << "Gano el jugador " << player2 << endl;
          fin = true;
        } else if (turno >= 25) {
          cout << "Empate" << endl;
          system("pause");
          fin = true;
        }
      } else {
        if (Gano() == player1) {
          cout << "Gano el jugador " << player1 << endl;
          fin = true;
        } else if (Gano() == player2) {
          cout << "Gano el jugador " << player2 << endl;
          fin = true;
        } else if (turno >= 9) {
          cout << "Empate" << endl;
          system("pause");
          fin = true;
        }
      }

      CambiarTurno();
      turno++;
      char posicion = '0';
      cout << "Ingresa tu posicion " << endl;
      cin >> posicion;
      cout << posicion;
      sockServer.SendPlayer(posicion);
      if (mododejuego != '5') {
        Input(posicion);
      } else {
        Input5(posicion);
      }
      
      if (mododejuego != '5') {
        Draw();
      } else {
        Draw5();
      }

      if (mododejuego == '5') {
        if (Gano5() == player1) {
          cout << "Gano el jugador " << player1 << endl;
          fin = true;
        } else if (Gano5() == player2) {
          cout << "Gano el jugador " << player2 << endl;
          fin = true;
        } else if (turno >= 25) {
          cout << "Empate" << endl;
          system("pause");
          fin = true;
        }
      } else {
        if (Gano() == player1) {
          cout << "Gano el jugador " << player1 << endl;
          fin = true;
        } else if (Gano() == player2) {
          cout << "Gano el jugador " << player2 << endl;
          fin = true;
        } else if (turno >= 9) {
          cout << "Empate" << endl;
          system("pause");
          fin = true;
        }
      }

      CambiarTurno();
      turno++;
      
      if (strcmp(recMessage, "salir") == 0 ||
          strcmp(sendMessage, "salir") == 0) {
        fin = true;
      }
      
    }
    sockServer.CloseConnection();
    
  } else if (opcion == 2) {
    cout << "IP:\n";
    cin >> ipAddress;
    ClientSocket sockClient;
    cout << "Conectando\n";
    sockClient.ConnectToServer(ipAddress.c_str(), port);
    cout << "\t Esperando\n";
    sockClient.RecvData(recMessage, STRLEN);
    mododejuego = recMessage[0];
    cout << "Ingresa tu simbolo " << endl;
    cin >> player1;
    sockClient.SendPlayer(player1);
    cout << "\t Esperando\n";
    sockClient.RecvData(recMessage, STRLEN);
    player2 = recMessage[0];
    cout << "Jugador 2: " << player2 << endl;
    player = player1;
    while (!fin) {
      if (mododejuego == '5') {
        Draw5();
      } else {
        Draw();
      }
      
      char posicion = '0';
      cout << "Ingresa tu posicion " << endl;
      cin >> posicion;
      cout << posicion;
      sockClient.SendPlayer(posicion);

      if (mododejuego != '5') {
        Input(posicion);
      } else {
        Input5(posicion);
      }

      if (mododejuego != '5') {
        Draw();
      } else {
        Draw5();
      }

      if (mododejuego == '5') {
        if (Gano5() == player1) {
          cout << "Gano el jugador " << player1 << endl;
          fin = true;
        } else if (Gano5() == player2) {
          cout << "Gano el jugador " << player2 << endl;
          fin = true;
        } else if (turno >= 25) {
          cout << "Empate" << endl;
          system("pause");
          fin = true;
        }
      } else {
        if (Gano() == player1) {
          cout << "Gano el jugador " << player1 << endl;
          fin = true;
        } else if (Gano() == player2) {
          cout << "Gano el jugador " << player2 << endl;
          fin = true;
        } else if (turno >= 9) {
          cout << "Empate" << endl;
          system("pause");
          fin = true;
        }
      }

      CambiarTurno();
      turno++;
      cout << "\t Esperando\n";
      sockClient.RecvData(recMessage, STRLEN);
      
      if (mododejuego != '5') {
        Input(recMessage[0]);
      } else {
        Input5(recMessage[0]);
      }
      
      if (mododejuego != '5') {
        Draw();
      } else {
        Draw5();
      }
      
      if (mododejuego == '5') {
        if (Gano5 () == player1) {
          cout << "Gano el jugador " << player1 << endl;
          fin = true;
        } else if (Gano5() == player2) {
          cout << "Gano el jugador " << player2 << endl;
          fin = true;
        } else if (turno >= 25) {
          cout << "Empate" << endl;
          system("pause");
          fin = true;
        }
      } else {
        if (Gano() == player1) {
          cout << "Gano el jugador " << player1 << endl;
          fin = true;
        } else if (Gano() == player2) {
          cout << "Gano el jugador " << player2 << endl;
          fin = true;
        } else if (turno >= 9) {
          cout << "Empate" << endl;
          system("pause");
          fin = true;
        }
      }
      
      CambiarTurno();
      turno++;
      
      if (strcmp(recMessage, "salir") == 0 ||
          strcmp(sendMessage, "salir") == 0) {
        fin = true;
      }
      
    }

    sockClient.CloseConnection();

  } else {
    exit(0);
  }

  return 0;
}

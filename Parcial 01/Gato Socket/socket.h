#include <iostream>
#include <winsock2.h>

using namespace std;

const int STRLEN =256; // Esto es el buffer

class Socket { // Es nuestra conexión
  protected :
    WSADATA wsaData;
    SOCKET mySocket;
    SOCKET myBackup; // Guarda el UDP
    SOCKET acceptSocket; // el socket al ser aceptado
    sockaddr_in myAddress; // La direccion reservada al connectarse
  
  public :
    Socket();
    ~Socket();
    
    bool SendData(char*);
    bool RecvData(char*, int);
    void CloseConnection();
    void GetAndSendMessage();
    void SendPlayer(char);
};

class ServerSocket : public Socket{
    public: 
      void Listen();
      void Bind(int _port);
      void StartHosting(int _port);
};

class ClientSocket : public Socket {
  
    public :
      void ConnectToServer (const char *_ipAddress, int _port);
};

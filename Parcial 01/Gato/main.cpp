#include <iostream>
#include <ctype.h>
using namespace std;
char matrix3[3][3] = { '1', '2', '3', '4', '5', '6', '7', '8', '9' };
char player1 = 'X';
char player2 = 'O';
char player = 'X';
void Draw() {
    // system("cls"); // Para windows
    system("clear"); // Para el linux
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            cout << matrix3[i][j] << " ";
        }
        cout << endl;
    }
}

void Input() {
    int a;
    cout << "Ingresa el número: ";
    cin >> a;

    if (a == 1)
        matrix3[0][0] = player;
    else if (a == 2)
        matrix3[0][1] = player;
    else if (a == 3)
        matrix3[0][2] = player;
    else if (a == 4)
        matrix3[1][0] = player;
    else if (a == 5)
        matrix3[1][1] = player;
    else if (a == 6)
        matrix3[1][2] = player;
    else if (a == 7)
        matrix3[2][0] = player;
    else if (a == 8)
        matrix3[2][1] = player;
    else if (a == 9)
        matrix3[2][2] = player;
}
void CambiarTurno() {
    if (player == player1)
        player = player2;
    else
        player = player1;
}

char Gano() {
    if (matrix3[0][0] == player1 && matrix3[0][1] == player1 && matrix3[0][2] == player1)
        return player1;
    if (matrix3[1][0] == player1 && matrix3[1][1] == player1 && matrix3[1][2] == player1)
        return player1;
    if (matrix3[2][0] == player1 && matrix3[2][1] == player1 && matrix3[2][2] == player1)
        return player1;

    if (matrix3[0][0] == player1 && matrix3[1][0] == player1 && matrix3[2][0] == player1)
        return player1;
    if (matrix3[0][1] == player1 && matrix3[1][1] == player1 && matrix3[2][1] == player1)
        return player1;
    if (matrix3[0][2] == player1 && matrix3[1][2] == player1 && matrix3[2][2] == player1)
        return player1;

    if (matrix3[0][0] == player1 && matrix3[1][1] == player1 && matrix3[2][2] == player1)
        return player1;
    if (matrix3[2][0] == player1 && matrix3[1][1] == player1 && matrix3[0][2] == player1)
        return player1;


    if (matrix3[0][0] == player2 && matrix3[0][1] == player2 && matrix3[0][2] == player2)
        return player2;
    if (matrix3[1][0] == player2 && matrix3[1][1] == player2 && matrix3[1][2] == player2)
        return player2;
    if (matrix3[2][0] == player2 && matrix3[2][1] == player2 && matrix3[2][2] == player2)
        return player2;

    if (matrix3[0][0] == player2 && matrix3[1][0] == player2 && matrix3[2][0] == player2)
        return player2;
    if (matrix3[0][1] == player2 && matrix3[1][1] == player2 && matrix3[2][1] == player2)
        return player2;
    if (matrix3[0][2] == player2 && matrix3[1][2] == player2 && matrix3[2][2] == player2)
        return player2;

    if (matrix3[0][0] == player2 && matrix3[1][1] == player2 && matrix3[2][2] == player2)
        return player2;
    if (matrix3[2][0] == player2 && matrix3[1][1] == player2 && matrix3[0][2] == player2)
        return player2;

    return '/';
}

int main() {
    cout << "Ingresa el simbolo para el jugador 1" << endl;
    cin >> player1;
    while (isdigit(player1)) {
      cout << "Ingresa el simbolo que no se un numero del 1 al 9 para el jugador 1" << endl;
      cin >> player1;
    }

    cout << "Ingresa el simbolo para el jugador 2" << endl;
    cin >> player2;
    while (isdigit(player2)) {
      cout << "Ingresa el simbolo que no se un numero del 1 al 9 para el jugador 2" << endl;
      cin >> player2;
    }
    player = player1;
    Draw();
    while (1) {
        Input();
        Draw();
        if (Gano() == player1) {
            cout << "Gano el jugador " << player1 << endl;
            break;
        } else if (Gano() == player2) {
            cout << "Gano el jugador " << player2 << endl;
            break;
        } else if (Gano() == "/") {
          cout << "Empate" << endl;
          break;
        }
        CambiarTurno();
    }
    // system("pause"); // windows
    int c = getchar(); // Para el linux
    return 0;
}

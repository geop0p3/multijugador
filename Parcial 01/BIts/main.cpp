#include <iostream>
#include <bitset>

using namespace std;
#define BitSize 8

int main () {
  bitset<BitSize> bitInicial;
  bitset<BitSize> bitSecundario(20);
  bitset<BitSize> bitString(string("00000100"));
  cout << "Bit Inicial " << bitInicial <<"\n";
  cout << "Bit Secundario " << bitSecundario <<"\n";
  cout << "Bit String " << bitString <<"\n";
  return 0;
}

#include <iostream>
#include "socket.h"
#include <windows.h>
#include <thread>
#include <bitset>
#include <string>
#define SizeBit 2

bool fin = false;
char recMessage[STRLEN];
char sendMessage[STRLEN];

bitset<SizeBit> b_L(string("00"));
bitset<SizeBit> b_R(string("01"));
bitset<SizeBit> b_U(string("10"));
bitset<SizeBit> b_D(string("11"));


using namespace std;

char* GetDirection(char* rm)
{
  string rec = rm;
  int pos = rec.find("=");
  string d = rec.substr(pos+1);

  if(d == "00") {
    return "Izquierda ";
  } else if(d == "01") {
    return "Derecha ";
  } else if(d == "10") {
    return "Arriba ";
  } else if(d == "11") {
    return "Abajo ";
  }
}


int main () {
  int port = 8080;
  int opcion = -1;
  string ipAddress;
  bool fin = false;
  char recMessage[STRLEN];
  char sendMessage[STRLEN];
  
  cout << "1) Server\n";
  cout << "2) Cliente\n";
  cout << "3) Salir\n";
  cin >> opcion;
  
  if (opcion == 1){
    ServerSocket sockServer;
    cout << "Hosting\n";
    sockServer.StartHosting(port);
    
    while (!fin){
      cout << "\t Esperando\n";
      sockServer.RecvData(recMessage,STRLEN);
      cout << " Recibido > " << recMessage << "\n";
      sockServer.GetAndSendMessage();
      if (strcmp (recMessage, "salir") == 0 || strcmp (sendMessage, "salir") == 0){
        fin = true;
      }
      GetDirection(recMessage)
      sockServer.CloseConnection();
      
    }
  } else if (opcion == 2){
    cout << "IP:\n";
    cin >> ipAddress;
    ClientSocket sockClient;
    cout << "Conectando\n";
    sockClient.ConnectToServer(ipAddress.c_str(), port);
    
    while (!fin){
      sockClient.GetAndSendMessage();
      cout << "\t Esperando\n";
      sockClient.RecvData(recMessage, STRLEN);
      cout << "Recibido > " << recMessage << "\n";
      if (strcmp (recMessage, "salir") == 0 || strcmp (sendMessage, "salir") == 0){
        fin = true;
      }
      GetDirection(recMessage)
    }
    
    sockClient.CloseConnection();
    
  } else {
    exit (0);
    
  }
  
  return 0;
}
